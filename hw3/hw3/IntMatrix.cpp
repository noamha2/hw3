#include <iostream>
#include "Auxiliaries.h";
#include "IntMatrix.h";
using namespace mtm;


#include "Auxiliaries.h"
#include "IntMatrix.h"

using namespace mtm;
using std::cout;
using std::endl;

//IntMatrix::Iterator::Iterator(const Iterator& iterator) : matrix(iterator.matrix),
//index(iterator.index), dimensions(iterator.dimensions) {}//can we remove this? is this default drivial?
//I think this is how the copy constructor is made. We point not to the pointer, but to the value the pointer points
//IntMatrix::Iterator::Iterator(int** matrix, Dimensions dimensions, int index) :
//	matrix(matrix), index(index), dimensions(dimensions) {}

IntMatrix::Iterator::Iterator(int** matrix, Dimensions dimensions, int index) :matrix(matrix), index(index), dimensions(dimensions) {}
int& IntMatrix::Iterator::operator* () const
{
	int matrix_col = dimensions.getCol();
	int row = index / matrix_col;
	int col = index - row * matrix_col;
	return matrix[row][col];//needs to check if it passed the reference
}

IntMatrix::Iterator& IntMatrix::Iterator::operator++()
{
	this->index++;
	return *this;
}

IntMatrix::Iterator IntMatrix::Iterator::operator++(int)
{
	IntMatrix::Iterator new_iterator(*this);
	operator++();
	return new_iterator;
}

bool IntMatrix::Iterator::operator==(const IntMatrix::Iterator& it) const
{
	//check if points to the same things;
	bool is_same_matrix = this->matrix == it.matrix;
	bool is_same_dimensions = this->dimensions == it.dimensions;
	bool is_same_index = this->index == it.index;
	if (is_same_matrix && is_same_dimensions && is_same_index)
	{
		return true;
	}
	return false;
}
bool IntMatrix::Iterator::operator!=(const IntMatrix::Iterator& it) const
{
	//check if points to the same things;
	bool is_same_matrix = this->matrix == it.matrix;
	bool is_same_dimensions = this->dimensions == it.dimensions;
	bool is_same_index = this->index == it.index;
	if (is_same_matrix && is_same_dimensions && is_same_index)
	{
		return false;
	}
	return true;
}

IntMatrix::Iterator& IntMatrix::Iterator::operator=(const Iterator& iterator)
{
	if (this == &iterator) return *this;
	//free values? same question as destructor

	//assign new values
	this->index = iterator.index;
	this->matrix = iterator.matrix;

	return *this;
}
IntMatrix::Iterator IntMatrix::begin() const
{
	IntMatrix::Iterator begin_iterator(this->matrix, this->dimensions, 0);
	return begin_iterator;
}
IntMatrix::Iterator IntMatrix::end() const
{
	int last_index = (this->dimensions.getRow()) * (this->dimensions.getCol()) - 1;
	IntMatrix::Iterator end_iterator(this->matrix, this->dimensions, last_index);
	return end_iterator;
}


int IntMatrix::height() const
{
	return dimensions.getRow();
}

int IntMatrix::width() const
{
	return dimensions.getCol();
}

int IntMatrix::size() const
{
	return matrix_size;
}

IntMatrix::IntMatrix(Dimensions dimensions, int initValue) :dimensions(dimensions),
matrix(new int* [dimensions.getRow()]), matrix_size(dimensions.getRow()* dimensions.getCol())
{
	matrix = createMatrix(dimensions, initValue);
}

IntMatrix::IntMatrix(const IntMatrix& mat) : dimensions(mat.dimensions),
matrix(createMatrix(mat.dimensions, 0)), matrix_size(mat.dimensions.getRow()* mat.dimensions.getCol())
{
	int height = mat.height();
	for (int i = 0; i < height; i++)
	{
		int width = mat.width();
		for (int j = 0; j < width; j++)
		{
			matrix[i][j] = mat.matrix[i][j];
		}
	}
}

IntMatrix::~IntMatrix()
{
	int rows_number = dimensions.getRow();
	for (int i = 0; i < rows_number; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;
}

IntMatrix& IntMatrix:: operator=(const IntMatrix& matrix_to_assign)
{
	if (this == &matrix_to_assign) return *this;
	//delete old data
	int rows_number = dimensions.getRow();
	for (int i = 0; i < rows_number; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;
	//assign new values

	dimensions = matrix_to_assign.dimensions;
	matrix_size = matrix_to_assign.size();

	matrix = createMatrix(dimensions, 0);
	//matrix = new int* [dimensions.getRow()];;
	int new_rows_number = dimensions.getRow();
	int new_columns_number = dimensions.getCol();
	for (int i = 0; i < rows_number; i++)
	{
		//matrix[i] = new int[new_col];
		for (int j = 0; j < new_columns_number; j++)
		{
			matrix[i][j] = matrix_to_assign.matrix[i][j];
		}
	}
	return *this;

}

IntMatrix IntMatrix::Identity(int size)
{
	//is it fine like that? all public and private(access modifiers) are fine?
	Dimensions dimensions(size, size);
	IntMatrix identity_intmatrix(dimensions);
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (i == j)
			{
				identity_intmatrix.matrix[i][j] = 1;
			}
		}
	}
	return identity_intmatrix;
}


IntMatrix IntMatrix::transpose() const
{
	Dimensions dimensions(this->width(), this->height());//use this or dimensions.getRow/getCol()?
	IntMatrix transposed_intmatrix(dimensions);
	int rows_number = dimensions.getRow();
	int columns_number = dimensions.getCol();
	for (int i = 0; i < rows_number; i++)
	{
		for (int j = 0; j < columns_number; j++)
		{
			transposed_intmatrix.matrix[i][j] = this->matrix[j][i];
		}
	}
	return transposed_intmatrix;
}

int** IntMatrix::createMatrix(Dimensions dimensions, int initValue = 0)
{
	int rows_number = dimensions.getRow();
	int** created_matrix = new int* [rows_number];
	int columns_number = dimensions.getCol();
	for (int i = 0; i < rows_number; i++)
	{
		created_matrix[i] = new int[columns_number];
		for (int j = 0; j < columns_number; j++)
		{
			created_matrix[i][j] = initValue;
		}
	}
	return created_matrix;
}

ostream& mtm::operator<<(ostream& os, const IntMatrix& matrix)
{
	//I do all this to avoid using one dimensional matrix, which is stupid. I flat the matrix.
	int height = matrix.height();
	int width = matrix.width();
	int* matrix_in_one_line = new int[height * width];
	int matrix_in_one_line_counter = 0;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			matrix_in_one_line[matrix_in_one_line_counter] = matrix.matrix[i][j];
			matrix_in_one_line_counter++;
		}

	}
	os << printMatrix(matrix_in_one_line, matrix.dimensions);
	delete[] matrix_in_one_line;
	return os;
}

IntMatrix IntMatrix::operator-(const IntMatrix& matrix_to_add) const
{
	IntMatrix new_matrix(this->dimensions);
	int row = this->dimensions.getRow();
	int col = this->dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			new_matrix.matrix[i][j] = this->matrix[i][j] - matrix_to_add.matrix[i][j];
		}
	}
	return new_matrix;
}

IntMatrix IntMatrix::operator-() const
{
	IntMatrix new_matrix(this->dimensions);
	int row = this->dimensions.getRow();
	int col = this->dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			new_matrix.matrix[i][j] = -this->matrix[i][j];
		}
	}
	return new_matrix;
}

IntMatrix& IntMatrix::operator+=(int number)
{
	int row = this->dimensions.getRow();
	int col = this->dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			this->matrix[i][j] += number;
		}
	}
	return *this;
}

IntMatrix IntMatrix::operator+(int number) const
{
	IntMatrix result = *this;
	result += number;
	return result;
}

IntMatrix IntMatrix::operator+(const IntMatrix& matrix_to_add) const
{
	IntMatrix new_matrix(this->dimensions);
	int row = this->dimensions.getRow();
	int col = this->dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			new_matrix.matrix[i][j] = this->matrix[i][j] + matrix_to_add.matrix[i][j];
		}
	}
	return new_matrix;
}

IntMatrix mtm::operator+(int number, const IntMatrix& first_matrix)
{
	return (first_matrix + number);
}

int& IntMatrix::operator()(int row, int col)
{
	return (this->matrix[row][col]);
}

const int& IntMatrix::operator()(int row, int col) const
{
	return (this->matrix[row][col]);
}

IntMatrix IntMatrix::operator==(int number) const
{
	IntMatrix result(this->dimensions);
	int row = result.dimensions.getRow();
	int col = result.dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (this->matrix[i][j] == number)
			{
				result.matrix[i][j] = 1;
			}
		}
	}
	return result;
}

IntMatrix IntMatrix::operator!=(int number) const
{
	IntMatrix final_result(this->dimensions, 1);
	IntMatrix equals(this->dimensions);
	equals = (*this == number);
	return (final_result - equals);
}

IntMatrix IntMatrix::operator<=(int number) const
{
	IntMatrix result(this->dimensions);
	int row = result.dimensions.getRow();
	int col = result.dimensions.getCol();
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (this->matrix[i][j] <= number)
			{
				result.matrix[i][j] = 1;
			}
		}
	}
	return result;
}

IntMatrix IntMatrix::operator<(int number) const
{
	IntMatrix first_result(this->dimensions);
	first_result = (*this <= number);
	IntMatrix second_result(this->dimensions);
	second_result = (*this == number);
	return (first_result - second_result);
}

IntMatrix IntMatrix::operator>(int number) const
{
	IntMatrix final_result(this->dimensions, 1);
	IntMatrix result(this->dimensions);
	result = (*this <= number);
	return (final_result - result);
}

IntMatrix IntMatrix::operator>=(int number) const
{
	IntMatrix first_result(this->dimensions);
	first_result = (*this > number);
	IntMatrix second_result(this->dimensions);
	second_result = (*this == number);
	return (first_result + second_result);
}

int main()
{
	int x = 4;
	int moti = x++;

	Dimensions dims(2, 4);
	IntMatrix mat_1(dims, 5);

	IntMatrix::Iterator it_end = mat_1.end();
	IntMatrix::Iterator it_begin = mat_1.begin();
	cout << mat_1 << endl;
	bool is = it_begin != it_end;
	cout << is << endl;

	cout << *it_end << endl;
	*it_end = 4;
	cout << *it_end << endl;
	cout << mat_1;
	IntMatrix mat_2 = mat_1;
	cout << mat_1 << endl;
	mat_1(1, 0) = 12;
	mat_1(0, 0) = 7;
	cout << mat_1 << endl;
	IntMatrix mat_7 = mat_1 <= 7;
	cout << mat_7 << endl;
	IntMatrix identity = IntMatrix::Identity(3);
	cout << identity << endl;
	IntMatrix transposed_mat_1 = mat_1.transpose();
	cout << transposed_mat_1 << endl;
	return 1;
}


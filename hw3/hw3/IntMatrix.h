#ifndef INT_MATRIX_H_
#define INT_MATRIX_H_

#include <iostream>
#include "Auxiliaries.h";IntMatrix operator
using mtm::Dimensions;
using std::cout;
using std::endl;
#include "Auxiliaries.h";
using mtm::Dimensions;
using std::ostream;

namespace mtm {
	class IntMatrix {
	private:
		int** matrix;
		int matrix_size;
		Dimensions dimensions;
		static int** createMatrix(Dimensions dimensions, int initValue);
	public:
		IntMatrix(Dimensions dimensions, int initValue = 0);
		IntMatrix(const IntMatrix& matrix);
		~IntMatrix();
		IntMatrix& operator=(const IntMatrix& matrix);
		IntMatrix& operator+=(int number);
		IntMatrix& operator-=(const IntMatrix& matrix);
		IntMatrix operator+(int number) const;
		IntMatrix operator+(const IntMatrix& matrix_to_add) const;
		IntMatrix operator-(const IntMatrix& matrix_to_add) const;
		IntMatrix operator-() const;
		IntMatrix operator!=(int number) const;
		IntMatrix operator==(int number) const;
		IntMatrix operator<=(int number) const;
		IntMatrix operator>=(int number) const;
		IntMatrix operator>(int number) const;
		IntMatrix operator<(int number) const;
		friend ostream& operator<<(ostream& os, const IntMatrix& matrix);
		int& operator()(const IntMatrix& matrix, Dimensions dimensions);
		const int& operator()(const IntMatrix& matrix, Dimensions dimensions) const;
		int height() const;
		int width() const;
		int size() const;
		int& operator()(int row, int col);
		const int& operator()(int row, int col) const;
		IntMatrix transpose() const;
		static IntMatrix Identity(int dimensions);
		class Iterator;
		Iterator begin() const;
		Iterator end() const;
		class mtm::IntMatrix::Iterator {
			int** matrix;;
			int index;
			Dimensions dimensions;
			Iterator(int** matrix, Dimensions dimensions, int index);
			friend class IntMatrix;
		public:
			int& operator*() const;
			Iterator& operator++();
			Iterator operator++(int);
			bool operator==(const Iterator& it) const;
			bool operator!=(const Iterator& it) const;
			Iterator(const Iterator& iterator) = default;
			Iterator& operator=(const Iterator& iterator);// = default; why default? it can do it itself?
			~Iterator() = default;
			//TODO: implement it? what the fuck do we need to put in? we don't want to free the referenced matrix.
			//Is there something to free?
		};
	};
	//IntMatrix operator+(const IntMatrix& matrix_to_add);//changed it to intaMatrix
	//IntMatrix operator+(const IntMatrix& first_matrix, int number);
	//IntMatrix operator-(const IntMatrix& first_matrix, const IntMatrix& second_matrix);
	IntMatrix operator+(int number, const IntMatrix& first_matrix);
	bool any(const IntMatrix& matrix);
	bool all(const IntMatrix& matrix);
}

#endif /* INT_MATRIX_ */
